#!/bin/bash
app_name_env="$1"
keep_version="$2"

if [[ -z $keep_version ]]; then
  echo "usage: ./cleanup.sh <app_name> <n_version_to_keep>"
  exit 1
elif [[ $keep_version -lt 2 ]]; then
  echo "minimal 2 version to keep"
  exit 1
else
  for old_release in $(ls /opt/ | sort -V | grep "${app_name_env}" | grep -v "^todo-application-dev$" | head -n-${keep_version:-3});
  do
    # echo "release: ${old_release}"
    [[ ! -z ${old_release} ]] && rm -vrf /opt/${old_release}
  done
fi