package com.example.springboot3todoapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot3TodoApplication {
  public static final String APP_VERSION = getPackageVersion();

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot3TodoApplication.class, args);
	}

	private static String getPackageVersion() {
    Package pkg = SpringBoot3TodoApplication.class.getPackage();
    String pkgVersion = pkg.getImplementationVersion();
    return pkgVersion;
  }

}
