package com.example.springboot3todoapplication.controllers;

import org.springframework.stereotype.Controller;

import com.example.springboot3todoapplication.SpringBoot3TodoApplication;
import com.example.springboot3todoapplication.services.TodoItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
    public static final String pkgVersion = SpringBoot3TodoApplication.APP_VERSION;

    @Autowired
    private TodoItemService todoItemService;

    @GetMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("todoItems", todoItemService.getAll());
        modelAndView.addObject("appVersion", pkgVersion);
        System.out.println("HomeController - Load appVersion: " + pkgVersion);
        return modelAndView;
    }

}
