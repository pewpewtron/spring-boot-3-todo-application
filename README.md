# Spring Boot 3 Todo Application

![spring boot todo application](./images/screenshot.png)

This is an ENTIRE application for Java Spring Boot built using:
- Spring Boot 3.0.0
- Spring Data JPA
- H2 Database
- Thymeleaf

**I walk through how to build this yourself on YouTube**: https://youtu.be/yqWtYKWWcpY

## Development Instructions

- `git clone https://gitlab.i-3.my.id:8443/demo/spring-boot-3-todo-application.git`
- `cd spring-boot-3-todo-application`
- open in favorite editor, or
- `mvnw spring-boot:run`
- open http://localhost:8080 and TODO away!

## LICENSE

MIT License